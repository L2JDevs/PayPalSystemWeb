<?php

$lang = array();

// Properties:
$lang['title'] = 'PayPal Donation';

// Login panel message:
$lang['login_info'] = 'Indicate the name of the character to which you will enter your reward:';
$lang['login_input'] = 'Character`s name';

// Login error message:
$lang['login_name_empty'] = 'Please enter the name of the character.';
$lang['login_name_online'] = 'The indicated character is currently connected, disconnect it to make the donation.';
$lang['login_name_error'] = 'The indicated character does not exist.';
$lang['login_name_done'] = 'Successfully verified character.';
$lang['login_error_01'] = 'ERROR 001 - Oops! An error has been found. Please try again later.';
$lang['login_error_02'] = 'ERROR 002 - Oops! An error has been found. Please try again later.';

// Donate panel message:
$lang['donate_title'] = '¡Help us grow!';
$lang['donate_info'] = 'The server is 100% funded by donations made by supporters like you. All contributions will be used directly to finance both its development and its infrastructure.';
$lang['donate_note'] = 'Once the donation is made, the character you have indicated above will get a reward.';
$lang['donate_acount'] = 'Amount to donate:';

// Done panel message:
$lang['done_title'] = '¡Donation Sent!';
$lang['done_info'] = 'Thank you very much for the donation made, it will be used directly to finance the development and infrastructure of the server.';
$lang['done_note'] = 'Please connect your account and verify that you have received your donation reward.';

// Single text:
$lang['confirm'] = 'Confirm and Proceed';
$lang['verify'] = 'Verify';

?>