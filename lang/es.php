<?php

$lang = array();

// Properties:
$lang['title'] = 'PayPal - Donaciones';

// Login panel message:
$lang['login_info'] = 'Indique a continuación el nombre del personaje al que ingresar su recompensa:';
$lang['login_input'] = 'Nombre del personaje';

// Login error message:
$lang['login_name_empty'] = 'Por favor, ingrese el nombre del personaje.';
$lang['login_name_online'] = 'El personaje indicado esta actualmente conectado, desconectelo para relizar la donación.';
$lang['login_name_error'] = 'El personaje indicado no existe.';
$lang['login_name_done'] = 'Personaje verificado correctamente.';
$lang['login_error_01'] = 'ERROR 001 - Oops! Se ha encontrado un error. Por favor intentelo de nuevo mas tarde.';
$lang['login_error_02'] = 'ERROR 002 - Oops! Se ha encontrado un error. Por favor intentelo de nuevo mas tarde.';

// Donate panel message:
$lang['donate_title'] = '¡Ayudenos a crecer!';
$lang['donate_info'] = 'El servidor está 100 % financiado con las donaciones realizadas por colaboradores como usted. Todo aporte se destinará directamente a financiar tanto su desarrollo como su infraestructura.';
$lang['donate_note'] = 'Una vez realizada la donación, el pesonaje que habeis indicado anteriormente obtendrá una recompensa.';
$lang['donate_acount'] = 'Cantidad a donar:';

// Done panel message:
$lang['done_title'] = '¡Donación Enviada!';
$lang['done_info'] = 'Muchas gracias por la donación realizada, será destinada directamente a financiar el desarrollo y la infraestructura del servidor.';
$lang['done_note'] = 'Por favor, conecte su cuenta y verifique que ha recibido la recompensa de su donación.';

// Single text:
$lang['confirm'] = 'Confirmar y Proceder';
$lang['verify'] = 'Verificar';

?>