<?php

/**
 * PayPal System
 * Configuration
 */

// Here we can define if we want to work on the sandbox or live
// Here you can find the Paypal ipn simulator https://developer.paypal.com/webapps/developer/applications/ipn_simulator
// NOTICE: IPN simulator will give invalid response back if this is set to 1

// Enabled SandBox for test system.
// Set "true" for test and "false" for live.
// Default: true
define("USE_SANDBOX", true);

if (USE_SANDBOX == true)
{
	// Test seller account
	$myPayPalEmail = 'YourSandboxTestPaypal@Account.com';
	$payPalURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
}
else
{
	// Fill your PayPal email below
	// This is where you will receive the donations
	$myPayPalEmail = 'YourPaypal@Account.com';
	$payPalURL = "https://www.paypal.com/cgi-bin/webscr";
}

// Define the currency you want to use for paypal
// You can find them here https://developer.paypal.com/docs/classic/api/currency_codes/
$currency_code = 'EUR';

// Define coin option, coin amount and donate amount.
$donate_amount = 10;

// Define the donation item_id.
$donate_item_id = 3470; // Gold Bar

// Turn error reporting on or of (true=on | false=off)
// Default: true
define("USE_REPORTING", true);

// Enabled reports
// Default: true
if (USE_REPORTING == true)
{
	// empty
}
else
{
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
}

// Use a delay when someone submit a form
// It will give a little bit protection against a brute force attack
// Enable or Disable loading delay: (true=on | false=off)
// Default: true
define("LOADING_DELAY", true);

// Total delay in seconds
// Default: 2
$delaytime = 2;
?>
