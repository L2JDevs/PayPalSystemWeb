<?php

/**
 * PayPal System
 * IPN Coins
 */

require "paypal.class.php";
require "configuration.php";
require "../connect.php";
require "../values.php";

$p = new paypal_class;
$p->paypal_url = $payPalURL;

if ($p->validate_ipn())
{
	if ($p->ipn_data['payment_status']=='Completed') 
	{
		// Gets the donated amount
		$amount = $p->ipn_data['mc_gross'] - $p->ipn_data['mc_fee'];
		
		// Get name from paypal ipn data
		$custom = $p->ipn_data['custom'];
		
		// Other Values
		$ip = $_SERVER['REMOTE_ADDR'];
		
		// INSERT VALUES (transaction_id, amount, character_name, char_name, date)
		$sqlLogPayPal = "INSERT INTO log_paypal_donations VALUES ('".esc($p->ipn_data['txn_id'])."', '".(float)$amount."', '".esc($custom)."', '".$charName."', 'current_timestamp()');";
		$link->query($sqlLogPayPal);
		
		// Gets the amount that the player donated
		$getamount = $p->ipn_data['mc_gross'];
		
		// Donate Rewards
		if ($getamount == $donate_amount)
		{
			$sqlItem = "SELECT * FROM items WHERE owner_id = '".$charId."' AND item_id = '".$donate_item_id."'";
			if ($stmt = mysqli_prepare($link, $sqlItem))
			{
				if (mysqli_stmt_execute($stmt))
				{
					mysqli_stmt_store_result($stmt);
					if (mysqli_stmt_num_rows($stmt) == 1)
					{
						$sqlItemUpdate = "UPDATE items SET count = (count + '".$donate_amount."') WHERE item_id = '".$donate_item_id."' AND owner_id = '".$charId."'";
						$link->query($sqlItemUpdate);
					}
					else
					{
						$objectId = 0;
						$sqlObjectId = "SELECT object_id FROM items ORDER BY object_id DESC LIMIT 1";
						$result = mysqli_query($link, $sqlObjectId);
						if (mysqli_num_rows($result) > 0)
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$objectId = $row["object_id"] + 1;
							}
						}
						
						$sqlItemAdd = "INSERT INTO items VALUES ('".$charId."', '".$objectId."', '".$donate_item_id."', '".$donate_amount."', 0, 'INVENTORY', 0, null, 0, 0, -1, -1, 0)";
						$link->query($sqlItemAdd);
					}
				}
			}
			
			mysqli_stmt_close($stmt);
		}
	}
}

function esc($str)
{
	global $db_link;
	return mysqli_real_escape_string($db_link, $str);
}
?>
