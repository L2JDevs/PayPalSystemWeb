<?php

// Include language file
include_once 'common.php';

// Done, go to index
header( "refresh:10; url=index.php" );

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; }
	</style>
	
</head>
<body>
	<div class="page-header">
		<h1><b><?php echo $lang['done_title'];?></b></h1>
		<h2><?php echo $lang['done_info'];?></h2>
		<h3>*<?php echo $lang['done_note'];?></h3>
		<br>
	</div>
</body>
</html>