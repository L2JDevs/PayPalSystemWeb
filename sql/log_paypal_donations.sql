-- ----------------------------
-- Table structure for `log_paypal_donations`
-- ----------------------------
DROP TABLE IF EXISTS `log_paypal_donations`;
CREATE TABLE `log_paypal_donations` (
  `transaction_id` varchar(64) NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  `char_name` varchar(45) NOT NULL DEFAULT '',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
