<?php

include_once 'common.php';
require "config/connect.php";
require "config/values.php";
require 'config/paypal/configuration.php';
 
// Local values
$name = "";
$charname_err = "";
$isDone = false;

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	if (empty(trim($_POST["name"])))
	{
		$charname_err = $lang['login_name_empty'];
	}
	else
	{
		$charName = trim($_POST["name"]);
	}
	
	if (empty($charname_err))
	{
		$sql = "SELECT charId, online FROM characters WHERE char_name = ?";
		if ($stmt = mysqli_prepare($link, $sql))
		{
			mysqli_stmt_bind_param($stmt, "s", $param_charname);
			$param_charname = $charName;
			
			if (mysqli_stmt_execute($stmt))
			{
				mysqli_stmt_store_result($stmt);
				if (mysqli_stmt_num_rows($stmt) >= 1)
				{
					$stmt->bind_result($charid, $state);
					if ($state == 0)
					{
						$_SESSION["charName"] = $charName;
						$_SESSION["charId"] = $charId;
						
						$isDone = true;
						
						$charname_err = $lang['login_name_done'];
					}
					else
					{
						$charname_err = $lang['login_name_online'];
					}
				}
				else
				{
					$charname_err = $lang['login_name_error'];
				}
			}
			else
			{
				$charname_err = $lang['login_error_02'];
			}
			
			mysqli_stmt_close($stmt);
			mysqli_close($link);
		}
		else
		{
			$charname_err = $lang['login_error_01'];
		}
	}
	else
	{
		$charname_err = $lang['login_name_empty'];
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
	<script type="text/javascript" src="js/conditions.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; width: 350px; padding: 20px; }
	</style>
</head>
<body>
	<div>
		<h2><?php echo $lang['title'];?></h2>
		<p><?php echo $lang['login_info'];?></p>
		
		<?php 
		
		if (!$isDone)
		{
			?>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
			<?php 
		}
		else
		{
			?>
			<form target="_blank" action="<?php echo $payPalURL?>" method="post" class="payPalForm">
			<?php 
		}
		
		?>
		
			<div class="form-group <?php echo (!empty($charname_err)) ? 'has-error' : 'has-success'; ?>">
				<input type="text" name="name" class="form-control" placeholder="<?php echo $lang['login_input'];?>" value="<?php echo $charName; ?>">
				<span class="help-block"><?php echo $charname_err; ?></span>
			</div>	
		
		<?php 
		
			if (!$isDone)
			{
				?>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="<?php echo $lang['verify'];?>">
				</div>
				<?php 
			}
			
		?>

			<!-- oke now lets show the donation options -->
			<!-- The PayPal coins Donation option list -->
			<input type="hidden" name="cmd" value="_donations" />
			<input type="hidden" name="item_name" value="Donation" />
			
			<!-- custom field that will be passed to paypal -->
			<input type="hidden" name="custom" value="<?php echo $charName?>">
			
			<!-- Your PayPal email -->
			<input type="hidden" name="business" value="<?php echo $myPayPalEmail?>" />
			<!-- PayPal will send an IPN notification to this URL -->
			<input type="hidden" name="notify_url" value="config/paypal/ipn_coins.php" />
			
			<!-- The return page to which the user is navigated after the donations is complete -->
			<input type="hidden" name="return" value="done.php" />
			
			<!-- Signifies that the transaction data will be passed to the return page by POST -->
			<input type="hidden" name="rm" value="2" />
			
			<!-- General configuration variables for the paypal landing page. Consult -->
			<!-- http://www.paypal.com/IntegrationCenter/ic_std-variable-ref-donate.html for more info -->
			<input type="hidden" name="no_note" value="1" />
			<input type="hidden" name="cbt" value="Go Back To The Site" />
			<input type="hidden" name="no_shipping" value="1" />
			<input type="hidden" name="lc" value="US" />
			<input type="hidden" name="currency_code" value="<?php echo $currency_code?>" />
			
			<!-- here the amount of the coins donation can be configured visible html only -->
			<b><?php echo $lang['donate_acount'];?></b>
			
			<br>
			<br>
			
			<input type="text" name="amount" value="<?php echo $donate_amount?>" />
			
			<br>
			<br> 

			<?php 
		
				if ($isDone)
				{
					?>
					<!-- Here you can change the image of the coins donation button  -->
					<input type="submit" name="bn" class="btn btn-primary" value="<?php echo $lang['confirm'];?>" />
					<br>
					<br>
					<?php 
				}
			
			?>
		</form>
		<p>
				<a href="?lang=en"><img src="images/flag/en.png"></a>
				<a href="?lang=es"><img src="images/flag/es.png"></a>
			</p>
	</div>
</body>
</html>